﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SRegCore.Models;
using SRegCore.Interface;

namespace SRegCore.Service
{
    public class DailyReportService : IDailyReportService
    {
        public DataTable GetReportByLeaveId(Int32 LeaveId)
        {
            DataTable dtUpdate = SqlHelp.GetDataTable($"Select * from Report Where LeaveId='{LeaveId}'");
            return dtUpdate;
        }
        public Boolean UpdateReport(Int32 LeaveId, DailyReportModel req)
        {
            string strUpdate = string.Empty;
            strUpdate += "UPDATE [Report]" + Environment.NewLine;

            strUpdate += "SET [Pref_Language] = '" + req.Pref_Language + "'" + Environment.NewLine;
            strUpdate += " ,[LeaveId] = '" + req.LeaveId + "'" + Environment.NewLine;
            strUpdate += " ,[Day] = '" + req.Day + "'" + Environment.NewLine;
            strUpdate += " ,[DutyIn] = '" + req.DutyIn + "'" + Environment.NewLine;
            strUpdate += " ,[am] = '" + req.am + "'" + Environment.NewLine;

            strUpdate += " ,[DutyOut] = '" + req.DutyOut + "'" + Environment.NewLine;
            strUpdate += " ,[pm] = '" + req.pm + "'" + Environment.NewLine;
            strUpdate += " ,[Date] = '" + req.Date + "'" + Environment.NewLine;

            strUpdate += " ,[Name] = '" + req.Name + "'" + Environment.NewLine;
            strUpdate += " ,[Designation] = '" + req.D_des + "'" + Environment.NewLine;
            strUpdate += " ,[Department] = '" + req.Dept + "'" + Environment.NewLine;
            strUpdate += " ,[org] = '" + req.org + "'" + Environment.NewLine;
            strUpdate += " ,[Absent] = '" + req.Absent + "'" + Environment.NewLine;
            strUpdate += " ,[dho] = '" + req.dho + "'" + Environment.NewLine;
            strUpdate += " ,[alloted] = '" + req.alloted + "'" + Environment.NewLine;
            strUpdate += " ,[MajorObserved] = '" + req.MajorObserved + "'" + Environment.NewLine;
            strUpdate += " ,[MajorRequirement] = '" + req.MajorRequirement + "'" + Environment.NewLine;
            strUpdate += " ,[AnySuggestion] = '" + req.Suggestion + "'" + Environment.NewLine;

            strUpdate += "Where ReportId='" + LeaveId.ToString() + "'" + Environment.NewLine;
            var result = SqlHelp.ForExecuteNonQuery(strUpdate);
            return result;
            // if (SqlHelp.ForExecuteNonQuery(strUpdate) == true)
            //{
            //    btnSubmit.Text = "Update";
            //    lblmsg.Text = "All detail succefully";
            //    Response.Redirect("index.aspx");
            //}
        }
        public DataTable CreateReport(DailyReportModel req)
        {

            // string strD_DDMMYY = req.D_DD + "/" + req.D_MM + "/" + req.D_YY;

            string strQry = "Declare @MasterId int" + Environment.NewLine;
            strQry += "INSERT INTO [Report]" + Environment.NewLine;
            strQry += "([Pref_Language],[LeaveId],[Day],[DutyIn]" + Environment.NewLine;
            strQry += ",[am],[DutyOut],[pm],[Date]" + Environment.NewLine;
            strQry += ",[Name],[D_des],[Dept],[org]" + Environment.NewLine;
            strQry += ",[Absent],[dho],[alloted],[MajorOberved]" + Environment.NewLine;
            strQry += ",[MajorRequirement],[Suggestion])" + Environment.NewLine;


            strQry += "Values('" + req.Pref_Language + "' ,'" + req.LeaveId + "','" + req.Day + "'," + Environment.NewLine;
            strQry += "'" + req.DutyIn + "','" + req.am + "', '" + req.DutyOut + "','" + req.pm + "' ," + Environment.NewLine;
            strQry += "'" + req.Name + "'," + Environment.NewLine;
            strQry += "'" + req.Date + "','" + req.Name + "'," + Environment.NewLine;
            strQry += "'" + req.D_des + "','" + req.Dept + "' ,'" + req.org + "' ,'" + req.Absent + "','" + req.dho + "'," + Environment.NewLine;
            strQry += "'" + req.alloted + "','" + req.MajorObserved + "','" + req.MajorRequirement + "','" + req.Suggestion + "')" + Environment.NewLine;
            strQry += " set @MasterId=@@IDENTITY" + Environment.NewLine;
            SqlHelp.ForExecuteNonQuery(strQry);
            strQry = "SELECT @MasterId as MaxId";
            DataTable dtID = SqlHelp.GetDataTable(strQry);
            return dtID;
            //if (dtID.Rows.Count > 0)
            //{
            //    Session["ID"] = dtID.Rows[0]["MaxId"].ToString();
            //    Email.EmailToAdmin("DailyReport", Mail_Admin());
            //    //Email.EmailToUser(Dpmail.Text, Mail_User(), "Online Ragistration : Hindi & English");

            //    Clear();
            //    Response.Redirect("index.aspx");
            //}
        }
    }
}
