﻿using SRegCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SRegCore.Interface
{
    public interface IDailyReportService
    {
        DataTable GetReportByLeaveId(Int32 LeaveId);
        Boolean UpdateReport(Int32 LeaveId, DailyReportModel req);
        DataTable CreateReport(DailyReportModel req);
    }
}
