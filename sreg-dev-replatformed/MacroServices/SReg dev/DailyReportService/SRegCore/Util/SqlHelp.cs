﻿using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

/// <summary>
/// Summary description for SqlHelp
/// </summary>
public class SqlHelp
{
    private static IConfiguration configuration;
    public static void SetConfiguration(IConfiguration configuration)
    {
        SqlHelp.configuration = configuration;
    }
    public SqlHelp(IConfiguration configuration)
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Private function For Get Connection string from WebConfig file
    /// </summary>
    /// <returns></returns>
    private static string ConnectionString
    {
        get
        {
            // string strConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ToString();
            // return strConnectionString;
            string strConnectionString = configuration["ConnectionString"];
            return strConnectionString;
        }
    }
    /// <summary>
    /// public function for get datatable from Database form Query
    /// </summary>
    /// <param name="strQry"></param>
    /// <returns></returns>
    public static DataTable GetDataTable(string strQry)
    {
        using (SqlConnection sCon = new SqlConnection(ConnectionString))
        {
            using (SqlDataAdapter sAdp = new SqlDataAdapter(strQry, sCon))
            {
                sCon.Open();
                DataTable dt = new DataTable();
                sAdp.Fill(dt);
                return dt;
            }
        }
    }
    /// <summary>
    /// public function for getdatareader form database from query(This is fast from datatable).
    /// </summary>
    /// <param name="strQry"></param>
    /// <returns></returns>
    public static async Task<SqlDataReader> GetDataReader(string strQry)
    {
        using (SqlConnection sCon = new SqlConnection(ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand(strQry, sCon))
            {
                sCon.Open();
                SqlDataReader sdr = await cmd.ExecuteReaderAsync();
                return sdr;
            }
        }
    }
    /// <summary>
    /// Public function for ExcecuteNonQuery(Insert,Update,Delete) in Database.
    /// </summary>
    /// <param name="strQry"></param>
    /// <returns></returns>
    public static bool ForExecuteNonQuery(string strQry)
    {
        bool retVal = false;
        using (SqlConnection sCon = new SqlConnection(ConnectionString))
        {
            using (SqlCommand sCmd = new SqlCommand(strQry, sCon))
            {
                try
                {
                    sCon.Open();
                    if (sCmd.ExecuteNonQuery() > 0)
                    {
                        retVal = true;
                    }
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
            }
        }
        return retVal;
    }
    /// <summary>
    /// Public function for getsinglevalue from database table.
    /// </summary>
    /// <param name="strQry"></param>
    /// <returns></returns>
    public static object GetSingleValue(string strQry)
    {
        using (SqlConnection sCon = new SqlConnection(ConnectionString))
        {
            using (SqlCommand sCmd = new SqlCommand(strQry, sCon))
            {
                object obj;
                try
                {
                    sCon.Open();
                    obj = sCmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    obj = -1;
                    throw (ex);
                }
                return obj;
            }
        }
    }

}
