using System;

namespace SRegCore.Models
{
    public class DailyReportModel
    {
        public string LeaveId { get; set; } 
        public string Pref_Language { get; set; } 
        public DateTime Day { get; set; } 
        public DateTime DutyIn { get; set; } 
        public string am { get; set; } 
        public DateTime DutyOut { get; set; } 
        public string pm { get; set; } 
        public DateTime Date { get; set; } 
        public string Name { get; set; } 
        public string D_des { get; set; } 
        public string Dept { get; set; } 
        public string org { get; set; } 
        public string Absent { get; set; } 
        public string dho { get; set; } 
        public string alloted { get; set; } 
        public string MajorObserved { get; set; } 
        public string MajorRequirement { get; set; } 
        public string Suggestion { get; set; } 
    }
}