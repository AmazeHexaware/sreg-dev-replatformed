using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SRegCore.Models;
using SRegCore.Interface;

namespace SRegCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DailyReportController : ControllerBase
    {
        private readonly IDailyReportService service;
        public DailyReportController(IDailyReportService service)
        {
            this.service = service;
        }
        // GET api/dailyreport
        [HttpGet]
        public IActionResult Get([FromQuery]int LeaveId)
        {
            DataTable dtUpdate = service.GetReportByLeaveId(LeaveId);
             return Ok(dtUpdate);
        }

        // PUT api/dailyreport/{LeaveId}
        [HttpPut("{LeaveId}")]
         public IActionResult Put(Int32 LeaveId, [FromBody] DailyReportModel req)
        {
            var result = service.UpdateReport(LeaveId, req);
            return Ok(result);
        }

        // POST api/dailyreport
        [HttpPost]
        public IActionResult Post(DailyReportModel req)
        {
            DataTable dtID = service.CreateReport(req);
            return Ok(dtID);
        }
    }
}
