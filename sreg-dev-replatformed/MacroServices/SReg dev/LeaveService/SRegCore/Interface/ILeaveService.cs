﻿using SRegCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRegCore.Interface
{
    public  interface ILeaveService
    {
        Boolean CreateLeave(LeaveModel req);
        Boolean DeleteLeave(Int32 LeaveId);
        Boolean ChangeLeaveStatus(Int32 LeaveId);
    }
}
