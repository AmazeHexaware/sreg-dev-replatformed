﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SRegCore.Interface;
using SRegCore.Service;
using SRegCore.Interface;
using SRegCore.Service;
using Swashbuckle.AspNetCore.Swagger;

namespace SRegCore
{
    public class Startup
    {
        public Startup(IHostingEnvironment env) // , IConfiguration configuration
        {
            IConfiguration config = AddConfiguration(env);
            Configuration = config;
            SqlHelp.SetConfiguration(Configuration);
        }

        public static IConfiguration AddConfiguration(IHostingEnvironment env) // , IConfiguration configuration
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables()
                .AddJsonFile($"settings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                builder.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
            }
            else
            {
                builder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            }
            builder.AddEnvironmentVariables();
            return builder.Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // Add Filters
            services
                .AddMvc(options =>
                {
                    options.Filters.Add(typeof(ExceptionFilter));
                }).AddControllersAsServices();

            // CORS Enabling
            services.AddCors(options => { options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials()); });
            services
                .AddSwaggerGen(options =>
                {
                    options.DescribeAllEnumsAsStrings();
                    options.SwaggerDoc("v1", new Info
                    {
                        Title = "SReg HTTP API",
                        Version = "v1",
                        Description = "The SReg Service HTTP API"
                    });
                });

            // Dependency Injection
            services.AddScoped<ILeaveService, LeaveService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            var pathBase = Configuration["PATH_BASE"];
            app.UseSwagger()
           .UseSwaggerUI(c =>
           {
               c.SwaggerEndpoint($"{ (!string.IsNullOrEmpty(pathBase) ? pathBase : string.Empty) }/swagger/v1/swagger.json", "SReg API V1");
               c.OAuthAppName("SReg Swagger UI");
           });
        }
    }
}
