using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Service
{
    public class ChangePasswordService : IChangePasswordService
    {
        public Boolean UpdatePassword(ChangePasswordModel req)
        {
            DataTable dtCheck = SqlHelp.GetDataTable($"select * from admin where adminID='{req.AdminId}' and password='{req.OldPasswrd}'");
            if (dtCheck.Rows.Count > 0)
            {
                var result = SqlHelp.ForExecuteNonQuery($"Update admin set password='{req.NewPasswrd}' where adminID='{req.AdminId}' and password='{req.OldPasswrd}'");
                return result;
            }
            else
            {
                throw new Exception("Old Password dosn't match !");
            }
        }
    }
}