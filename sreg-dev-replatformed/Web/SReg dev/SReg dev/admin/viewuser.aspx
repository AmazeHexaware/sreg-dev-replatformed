<%@ Page Title="" Language="C#" MasterPageFile="~/admin/mymaster.master" AutoEventWireup="true"
    CodeFile="viewuser.aspx.cs" Inherits="admin_viewuser" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" 
        style="color: #008000; font-size: 17pt; font-style: italic;">
        <tr>
            <td align="left">
                Employee Ragistration Information :-&nbsp;
                <asp:Label ID="lblMsg" runat="server" Text="[lblMsg]" ForeColor="#CC0000" 
                    Visible="False"></asp:Label>
                                                        </td>
        </tr>
        <tr>
            <td align="left">
                Employee Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtName" CssClass="formtext_012" runat="server" MaxLength="100"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;<asp:Button 
                    ID="Btnsearch" runat="server" Text="Search" onclick="Btnsearch_Click1" />
                <asp:Button ID="Btnadd" runat="server" Text="Add" onclick="Btnadd_Click1" />
                <asp:GridView ID="gvRecord" runat="server" 
                    AutoGenerateColumns="False" Width="100%"
                    OnRowCommand="gvRecord_RowCommand" OnRowDeleting="gvRecord_RowDeleting" EmptyDataText="No Records Available"
                    AllowPaging="True" OnPageIndexChanging="gvRecord_PageIndexChanging" PageSize="40"
                    BackColor="#D6D6D6" onrowcreated="gvRecord_RowCreated" 
                    onselectedindexchanged="gvRecord_SelectedIndexChanged" ShowFooter="True" 
                    DataSourceID="SqlDataSource1" >
                   
                    <Columns>
                        <asp:BoundField HeaderText="Name" DataField="Name" 
                            SortExpression="Name" />
                        <asp:BoundField HeaderText="RagistrationId" DataField="RagistrationId" 
                            SortExpression="RagistrationId" />
                        <asp:BoundField HeaderText="Category" DataField="Category" 
                            SortExpression="Category" />
                        <asp:BoundField HeaderText="BankNo" DataField="BankNo" 
                            SortExpression="BankNo" />
                        <asp:BoundField HeaderText="SalaryDate" DataField="SalaryDate" 
                            SortExpression="SalaryDate" />
                        <asp:BoundField HeaderText="SalaryAmount" DataField="SalaryAmount" 
                            SortExpression="SalaryAmount" />
                        <%--<asp:BoundField HeaderText="Fax #" DataField="Fax" />--%>
                        <asp:BoundField HeaderText="Address" DataField="Address" 
                            SortExpression="Address" />
                       
                        <asp:BoundField HeaderText="FName" DataField="FName" SortExpression="FName" />
                        <asp:BoundField HeaderText="Sex" DataField="Sex" SortExpression="Sex" />
                        <asp:BoundField HeaderText="PhotoName" DataField="PhotoName" 
                            SortExpression="PhotoName" />
                        <asp:BoundField HeaderText="BankName" DataField="BankName" 
                            SortExpression="BankName" />
                        <asp:BoundField HeaderText="MobileNo" DataField="MobileNo" 
                            SortExpression="MobileNo" />
                        <asp:BoundField HeaderText="LandlineNo" DataField="LandlineNo" 
                            SortExpression="LandlineNo" />
                        <asp:BoundField HeaderText="State" DataField="State" 
                            SortExpression="State" />
                         <asp:BoundField HeaderText="Designation" DataField="Designation" 
                            SortExpression="Designation" />
                          <asp:BoundField HeaderText="PinCode" DataField="PinCode" 
                            SortExpression="PinCode" />
                           <asp:BoundField HeaderText="City" DataField="City" 
                            SortExpression="City" />
                           <asp:BoundField HeaderText="DOB" DataField="DOB" SortExpression="DOB" />
                          <asp:BoundField DataField="DOJ" HeaderText="DOJ" SortExpression="DOJ" />
                        <asp:BoundField DataField="Dpmail" HeaderText="Dpmail" 
                            SortExpression="Dpmail" />
                        <asp:BoundField DataField="mail" HeaderText="mail" SortExpression="mail" />
                        <asp:BoundField DataField="Nationality" HeaderText="Nationality" 
                            SortExpression="Nationality" />
                        <asp:BoundField DataField="EmpNo" HeaderText="EmpNo" 
                            SortExpression="EmpNo" />
                        <asp:BoundField DataField="Department" HeaderText="Department" 
                            SortExpression="Department" />
                        <asp:BoundField DataField="S_Div" HeaderText="S_Div" 
                            SortExpression="S_Div" />
                        <asp:BoundField DataField="S_Per" HeaderText="S_Per" SortExpression="S_Per" />
                        <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                        <asp:BoundField DataField="total" HeaderText="total" SortExpression="total" />
                        <asp:BoundField DataField="oname" HeaderText="oname" SortExpression="oname" />
                        <asp:BoundField DataField="S_BU" HeaderText="S_BU" SortExpression="S_BU" />
                        <asp:BoundField DataField="S_OS" HeaderText="S_OS" SortExpression="S_OS" />
                        <asp:BoundField DataField="SS_Div" HeaderText="SS_Div" 
                            SortExpression="SS_Div" />
                        <asp:BoundField DataField="SS_Per" HeaderText="SS_Per" 
                            SortExpression="SS_Per" />
                        <asp:BoundField DataField="SS_OS" HeaderText="SS_OS" 
                            SortExpression="SS_OS" />
                        <asp:BoundField DataField="SS_BU" HeaderText="SS_BU" 
                            SortExpression="SS_BU" />
                        <asp:BoundField DataField="Pref_Language" HeaderText="Pref_Language" 
                            SortExpression="Pref_Language" />
                        <asp:BoundField DataField="RMob2" HeaderText="RMob2" 
                            SortExpression="RMob2" />
                        <asp:BoundField DataField="Place1" HeaderText="Place1" 
                            SortExpression="Place1" />
                        <asp:BoundField DataField="RName" HeaderText="RName" 
                            SortExpression="RName" />
                        <asp:BoundField DataField="RWork1" HeaderText="RWork1" 
                            SortExpression="RWork1" />
                        <asp:BoundField DataField="RMob1" HeaderText="RMob1" 
                            SortExpression="RMob1" />
                        <asp:BoundField DataField="RPlace1" HeaderText="RPlace1" 
                            SortExpression="RPlace1" />
                        <asp:BoundField DataField="RName2" HeaderText="RName2" 
                            SortExpression="RName2" />
                        <asp:BoundField DataField="RWork2" HeaderText="RWork2" 
                            SortExpression="RWork2" />
                        <asp:BoundField DataField="RPlace2" HeaderText="RPlace2" 
                            SortExpression="RPlace2" />
                        <asp:BoundField DataField="Work" HeaderText="Work" SortExpression="Work" />
                        <asp:BoundField DataField="Position" HeaderText="Position" 
                            SortExpression="Position" />
                        <asp:BoundField DataField="Mob1" HeaderText="Mob1" SortExpression="Mob1" />
                        <asp:BoundField DataField="Work2" HeaderText="Work2" 
                            SortExpression="Work2" />
                        <asp:BoundField DataField="Position2" HeaderText="Position2" 
                            SortExpression="Position2" />
                        <asp:BoundField DataField="Mobile2" HeaderText="Mobile2" 
                            SortExpression="Mobile2" />
                        <asp:BoundField DataField="Place2" HeaderText="Place2" 
                            SortExpression="Place2" />
                        <asp:BoundField DataField="Religion" HeaderText="Religion" 
                            SortExpression="Religion" />
                    </Columns>
                     <HeaderStyle CssClass="grid_header" HorizontalAlign="Center" BackColor="#F8F0A6"
                        BorderColor="#000000" Height="25px" ForeColor="#000000" />
                    <PagerStyle CssClass="pagerLink" HorizontalAlign="Center" Font-Bold="true" />
                    <RowStyle CssClass="grdText" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="Red" Font-Size="Medium" Font-Bold="true"
                        Font-Names="Arial" />
                    <AlternatingRowStyle BackColor="#E5E5E5" />
                </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:Dsr.bakConnectionString3 %>" 
                    
                    SelectCommand="SELECT [Name], [RagistrationId], [Category], [BankNo], [SalaryDate], [SalaryAmount], [Address], [FName], [Sex], [PhotoName], [BankName], [MobileNo], [LandlineNo], [State], [Designation], [PinCode], [City], [DOB], [DOJ], [Dpmail], [mail], [Nationality], [EmpNo], [Department], [S_Div], [S_Per], [Type], [total], [oname], [S_BU], [S_OS], [SS_Div], [SS_Per], [SS_OS], [SS_BU], [Pref_Language], [RMob2], [Place1], [RName], [RWork1], [RMob1], [RPlace1], [RName2], [RWork2], [RPlace2], [Work], [Position], [Mob1], [Work2], [Position2], [Mobile2], [Place2], [Religion] FROM [Ragistration]" 
                    DeleteCommand="DELETE FROM Ragistration WHERE (RagistrationId = @RagistrationId)">
                </asp:SqlDataSource>
                    <%--  <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                     SelectCommand="&quot;SELECT RagistrationID,Category,BankNo,SalaryDate,SalaryAmount,BankName,Name,FName,Sex,PhotoName,Address,City,PinCode,Designation,State,LandlineNo,MobileNo,DOB,DOJ,Nationality,EmpNo,Department,Type,TotalLeave,oname,S_BU,S_OS,S_Per,S_Div,SS_BU,SS_OS,SS_Per,SS_Div,Pref_Language,RName,RWork1,RMob1,RPlace1,RName2,RWork2,RMob2,RPlace2,Work1,Mob1,Place1,Position,Work2,Mobile21,Place2,Position2,Religon,Mail,Dpmail FROM Ragistration WHERE (RagistrationID=@RagistrationID)&quot;"
                    DeleteCommand="&quot;DELETE FROM RagistrationID WHERE (RagistrationID=@RagistrationID)&quot;" 
                    UpdateCommand="&quot;UPDATE Ragistration SET Category=@Category,BankNo=@BankNo,SalaryDate=@SalaryDate,SalaryAmount=@SalaryAmount,BankName=@BankName,Name=@Name,FName=@FName,Sex=@Sex,PhotoName=@PhotoName,Address=@Address,City=@City,PinCode=PinCode,Designation=@Designation,State=@State,LandlineNo=@LandlineNo,DOB=@DOB,DOJ=@DOJ,Nationality=@Nationality,EmpNo=@EmpNo,Department=@Department,Type=@Type,oname=@oname,S_BU=@S_BU,S_OS=@S_OS,S_Per=@S_Per,S_Div=@S_Div,SS_BU=@SS_BU,SS_OS=@SS_OS,SS_Per=@SS_Per,SS_Div=@SS_Div,RName=@RName,RWork1=@RWork1,RMob1=@RMob1,RPlace1=@RPlace1,Rname2=@Rname2,RWork2=@RWork2,RMob2=@RMob2,RPlace2=@RPlace2,Work=@#Work,Postion=@Postion,Mob1=@Mob1,Place1=@Place1,Work2=@Work2,Position2=@Position2,Mobile2=@Mobile2,Place2=@Place2,Religion=@Religion,Mail=@Mail,Dpmail=@Dpmail  WHERE (RagistrationID=@RagistrationID)&quot;&gt;" 
                    onselecting="SqlDataSource1_Selecting">
                    <DeleteParameters>
                                    <asp:Parameter Name ="RagistrationID" />                              
                                </DeleteParameters>
                               <SelectParameters>
                                    <asp:ControlParameter ControlID="txtRagistrationID" Name="RagistrationID" PropertyName= "Text" Type="String"/>
                                </SelectParameters>
                                 <UpdateParameters>
                                    <asp:Parameter Name="Category" /> 
                                    <asp:Parameter Name="BankNo" />
                                    <asp:Parameter Name="SalaryDate" />
                                    <asp:Parameter Name="SalaryAmount" />
                                    <asp:Parameter Name="BankName" />
                                    <asp:Parameter Name="Name" />
                                    <asp:Parameter Name="FName" />
                                    <asp:Parameter Name="Sex" />
                                    <asp:Parameter Name="City" />
                                    <asp:Parameter Name="State" />
                                    <asp:Parameter Name="PhotoName"/>
                                    <asp:Parameter Name="Address"/>
                                    <asp:Parameter Name="PinCode"/>
                                    <asp:Parameter Name="LandlineNo"/>
                                    <asp:Parameter Name="Desigantion"/>
                                     <asp:Parameter Name="MobileNo"/>
                                      <asp:Parameter Name="DOJ"/>
                                       <asp:Parameter Name="DOB"/>
                                        <asp:Parameter Name="Nationality"/>
                                         <asp:Parameter Name="EmpNo"/>
                                          <asp:Parameter Name="Department"/>
                                           <asp:Parameter Name="Type"/>
                                            <asp:Parameter Name="TotalLeave"/>
                                             <asp:Parameter Name="oname"/>
                                              <asp:Parameter Name="S_BU"/>
                                               <asp:Parameter Name="S_OS"/>
                                                <asp:Parameter Name="S_Per"/>
                                                 <asp:Parameter Name="S_Div"/>
                                                 <asp:Parameter Name="SS_BU"/>
                                               <asp:Parameter Name="SS_OS"/>
                                                <asp:Parameter Name="SS_Per"/>
                                                 <asp:Parameter Name="SS_Div"/>
                                                  <asp:Parameter Name="RName"/>
                                                   <asp:Parameter Name="RWork1"/>
                                                   <asp:Parameter Name="RMob1"/>
                                                   <asp:Parameter Name="RPlace1"/>
                                                   <asp:Parameter Name="RName2"/>
                                                    <asp:Parameter Name="RWork2"/>
                                                   <asp:Parameter Name="RMob2"/>
                                                   <asp:Parameter Name="RPlace2"/>
                                                   <asp:Parameter Name="Work"/>
                                                    <asp:Parameter Name="Position"/>
                                                   <asp:Parameter Name="Mob1"/>
                                                   <asp:Parameter Name="Place1"/>
                                                   <asp:Parameter Name="Work2"/>
                                                    <asp:Parameter Name="Position2"/>
                                                   <asp:Parameter Name="Mobile2"/>
                                                   <asp:Parameter Name="Place2"/>
                                                   <asp:Parameter Name="Religion"/>
                                                    <asp:Parameter Name="Drpmail"/>
                                                   <asp:Parameter Name="Mail"/>
                                                   
                                
                                
                                
                                </UpdateParameters>
                
                </asp:SqlDataSource>
                    --%>        
                </td>
        </tr>
        </table>
</asp:Content>
