using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeaveController : ControllerBase
    {
        private readonly ILeaveService service;
        public LeaveController(ILeaveService service)
        {
            this.service = service;
        }
        // POST api/leave
        [HttpPost]
        public IActionResult Post(LeaveModel req)
        {
            var result = service.CreateLeave(req);
            return Ok(result);
        }

        // DELETE api/leave
        [HttpDelete("{LeaveId}")]
        public IActionResult Delete(Int32 LeaveId)
        {
            var result = service.DeleteLeave(LeaveId);
            return Ok(result);
        }

         // DELETE api/leave
        [HttpPatch("{LeaveId}")]
        public IActionResult Patch(Int32 LeaveId)
        {
            var result = service.ChangeLeaveStatus(LeaveId);
            return Ok(result);
        }
    }
}
