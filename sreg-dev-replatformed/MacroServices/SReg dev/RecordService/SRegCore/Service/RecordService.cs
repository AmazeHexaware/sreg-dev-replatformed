﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Service
{
    public class RecordService : IRecordService
    {
        public Boolean UpdateRecord(Int32 Id, RecordModel req)
        {
            string strUpdate = string.Empty;
            strUpdate += "UPDATE [Record]" + Environment.NewLine;
            strUpdate += "SET [Pref_Language] = '" + req.Pref_Language + "'" + Environment.NewLine;
            strUpdate += " ,[RecordId] = '" + req.RecordId + "'" + Environment.NewLine;
            strUpdate += " ,[Cname] = '" + req.Cname + "'" + Environment.NewLine;
            strUpdate += " ,[Branch] = '" + req.Branch + "'" + Environment.NewLine;

            strUpdate += " ,[ContactPerson] = '" + req.ContactPerson + "'" + Environment.NewLine;
            strUpdate += ",[Date] = '" + req.Date + "'" + Environment.NewLine;
            strUpdate += ",[Designation] = '" + req.Designation.ToString() + "'" + Environment.NewLine;
            strUpdate += " ,[Sector1] = '" + req.Sector1 + "'" + Environment.NewLine;

            strUpdate += " ,[Sector2] = '" + req.Sector2 + "'" + Environment.NewLine;
            strUpdate += " ,[LandlineNo0] = '" + req.LandlineNo0 + "'" + Environment.NewLine;
            strUpdate += " ,[LandlineNo1] = '" + req.LandlineNo1 + "'" + Environment.NewLine;
            strUpdate += " ,[MobileNo0] = '" + req.MobileNo0 + "'" + Environment.NewLine;
            strUpdate += " ,[MobileNo1] = '" + req.MobileNo1 + "'" + Environment.NewLine;
            strUpdate += " ,[MobileNo2] = '" + req.MobileNo2 + "'" + Environment.NewLine;

            strUpdate += ",[FaxNo] = '" + req.FaxNo + "'" + Environment.NewLine;

            strUpdate += " ,[Address] = '" + req.Address + "'" + Environment.NewLine;
            strUpdate += " ,[City] = '" + req.City + "'" + Environment.NewLine;
            strUpdate += " ,[State] = '" + req.State + "'" + Environment.NewLine;
            strUpdate += " ,[EmailId] = '" + req.EmailId + "'" + Environment.NewLine;

            strUpdate += ",[Website] = '" + req.Website + "'" + Environment.NewLine;
            strUpdate += ",[Remark1] = '" + req.Remark1 + "'" + Environment.NewLine;
            strUpdate += ",[Remark2] = '" + req.Remark2 + "'" + Environment.NewLine;
            strUpdate += ",[Remark3] = '" + req.Remark3 + "'" + Environment.NewLine;
            strUpdate += $"Where RecordId='{Id}'" + Environment.NewLine;

            var result = SqlHelp.ForExecuteNonQuery(strUpdate);
            return result;
        }


        public Boolean CreateRecord(RecordModel req)
        {

            string strQry = "Declare @MasterId int" + Environment.NewLine;
            strQry += "INSERT INTO [Record]" + Environment.NewLine;
            strQry += " ([rbtnPref_Language],[RecordId],[Date],[Cname]" + Environment.NewLine;
            strQry += ",[Branch],[ContactPerson],[Designation],[Sector1]" + Environment.NewLine;
            strQry += ",[Sector2],[LandLineNo0 ],[LandLineNo1 ],[MobileNo0]" + Environment.NewLine;
            strQry += ",[MobileNo1],[MobileNo2],[FaxNo],[Address]" + Environment.NewLine;
            strQry += ",[City],[State],[EmailId],[Website]," + Environment.NewLine;
            strQry += " ,[Remark1],[Remark2],[Remark3])" + Environment.NewLine;
            strQry += "Values('" + req.Pref_Language + "','" + req.RecordId + "','" + req.Date + "', + ," + Environment.NewLine;
            strQry += "'" + req.Cname + "','" + req.Branch + "','" + req.ContactPerson + "'," + Environment.NewLine;
            strQry += "'" + req.Designation + "','" + req.Sector1 + "'," + Environment.NewLine;
            strQry += "'" + req.Sector2 + "','" + req.LandlineNo0 + "'," + Environment.NewLine;
            strQry += "'" + req.LandlineNo1 + "','" + req.MobileNo0 + "','" + req.MobileNo1 + "'," + Environment.NewLine;
            strQry += "'" + req.FaxNo + "','" + req.Address + "'," + Environment.NewLine;
            strQry += "'" + req.City + "','" + req.State + "'," + Environment.NewLine;
            strQry += "'" + req.EmailId + "','" + req.Website + "'," + Environment.NewLine;
            strQry += "'" + req.Remark1 + "','" + req.Remark2 + "','" + req.Remark3 + "')" + Environment.NewLine;

            var result = SqlHelp.ForExecuteNonQuery(strQry);
            return result;
        }
    }
}
