using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Service
{
    public class RegistrationService : IRegistrationService
    {
        public DataTable GetRegistration(int RagistrationId)
        {
            DataTable dtUpdate = SqlHelp.GetDataTable($"Select * from Ragistration Where RagistrationId='{RagistrationId}'");
            return dtUpdate;
        }

        public DataTable UpdateRegistration(RegistrationModel req)
        {
            string password = GeneralUtility.GetRandomPassword(6);
			
            string strQry = "Declare @MasterId int" + Environment.NewLine;
            strQry += "INSERT INTO [Ragistration]" + Environment.NewLine;
            strQry += " ([Pref_Language],[Category],[Password],[RagistrationId]" + Environment.NewLine;
            strQry += ",[BankNo],[SalaryDate],[SalaryAmount],[BankName]" + Environment.NewLine;
            strQry += ",[Name],[FName],[Sex],[PhotoName]" + Environment.NewLine;
            strQry += ",[Dpmail],[mail],[Address],[City]" + Environment.NewLine;
            strQry += ",[PinCode],[State],[LandlineNo],[MobileNo]" + Environment.NewLine;
            strQry += ",[DOB],[DOJ],[Religion],[Nationality]" + Environment.NewLine;
            strQry += ",[Department],[org],[type],[total],[Posit]" + Environment.NewLine;
            strQry += ",[S_BU],[S_OS],[S_Per],[S_Div]" + Environment.NewLine;
            strQry += ",[SS_BU],[SS_OS],[SS_Per],[SS_Div]" + Environment.NewLine;
            strQry += ",[Work],[Position],[Mob1],[Place1],[Work2],[Position2],[Mobile2],[Place2]" + Environment.NewLine;
            strQry += ",[RName1],[RWork1],[RMob1],[RPlace1],[Rname2],[RWork2],[RMob2],[RPlace2])" + Environment.NewLine;
            strQry += "Values('" + req.Pref_Language + "','" + req.Category + "','" + req.RagistrationId + "','" + req.BankNo + "'," + Environment.NewLine;
            strQry += "'" + req.SalaryDate + "','" + req.SalaryAmount + "','" + req.BankName + "'," + Environment.NewLine;
            strQry += "'" + req.Name + "','" + req.FName + "','" + req.Sex + "'," + Environment.NewLine;
            strQry += "'" + req.PhotoName + "','" + req.Dpmail + "','" + req.mail + "','" + req.Address + "'," + Environment.NewLine;
            strQry += "'" + req.City + "','" + req.PinCode + "','" + req.State + "'," + Environment.NewLine;
            strQry += "'" + req.LandlineNo + "','" + req.MobileNo + "','" + req.DOB + "','" + req.DOJ + "'," + Environment.NewLine;
            strQry += "'" + req.Religon + "','" + req.Nationality + "','" + req.Department + "'," + Environment.NewLine;

            strQry += "'" + req.org + "','" + req.type + "','" + req.total + "','" + req.Posit + "'," + Environment.NewLine;
            strQry += "'" + req.S_BU + "','" + req.S_OS + "','" + req.S_Per + "'," + Environment.NewLine;
            strQry += "'" + req.S_Div + "','" + req.SS_BU + "','" + req.SS_OS + "'," + Environment.NewLine;
            strQry += "'" + req.SS_Div + "','" + req.SS_Per + "'," + Environment.NewLine;
            strQry += "'" + req.Work1 + "','" + req.Position + "'," + Environment.NewLine;
            strQry += "'" + req.Mob1 + "','" + req.Place1 + "','" + req.Work2 + "'," + Environment.NewLine;
            strQry += "'" + req.Position2 + "','" + req.Mobile2 + "','" + req.Place2 + "'," + Environment.NewLine;
            strQry += "'" + req.RName + "','" + req.RWork1 + "','" + req.RMob1 + "'," + Environment.NewLine;
            strQry += "'" + req.RPlace1 + "','" + req.RName2 + "','" + req.RWork2 + "'," + Environment.NewLine;
            strQry += "'" + req.RMob2 + "','" + req.RPlace2 + "')" + Environment.NewLine;
            strQry += " set @MasterId=@@IDENTITY" + Environment.NewLine;
            SqlHelp.ForExecuteNonQuery(strQry);
            strQry = "SELECT @MasterId as MaxId";
            DataTable dtID = SqlHelp.GetDataTable(strQry);
           
            return dtID;
        }
               
        public Boolean CreateRegistration(RegistrationModel req)
        {
            string strUpdate = string.Empty;
            strUpdate += "UPDATE [Ragistration]" + Environment.NewLine;
            strUpdate += "SET[Pref_Language] = '" + req.Pref_Language + "'" + Environment.NewLine;
            strUpdate += ", [Category] = '" + req.Category + "'" + Environment.NewLine;
            strUpdate += ",[Password] = '" + req.Passwrd + "'" + Environment.NewLine;
            strUpdate += ",[RagistrationId] = '" + req.RagistrationId + "'" + Environment.NewLine;
            strUpdate += ",[BankNo] = '" + req.BankNo + "'" + Environment.NewLine;
            strUpdate += ",[S_DDMMYY] = '" + req.SalaryDate + "'" + Environment.NewLine;
            strUpdate += ",[SalaryAmount] = '" + req.SalaryAmount + "'" + Environment.NewLine;
            strUpdate += ",[BankName] = '" + req.BankName.ToString() + "'" + Environment.NewLine;
            strUpdate += ",[Name] = '" + req.Name + "'" + Environment.NewLine;

            strUpdate += " ,[FName] = '" + req.FName + "'" + Environment.NewLine;
            strUpdate += " ,[Sex] = '" + req.Sex + "'" + Environment.NewLine;
            strUpdate += " ,[PhotoName] = '" + req.PhotoName + "'" + Environment.NewLine;
            strUpdate += ",[mail] = '" + req.mail + "'" + Environment.NewLine;
            strUpdate += ",[Dpmail] = '" + req.Dpmail + "'" + Environment.NewLine;

            strUpdate += " ,[Address] = '" + req.Address + "'" + Environment.NewLine;
            strUpdate += " ,[City] = '" + req.City + "'" + Environment.NewLine;
            strUpdate += " ,[PinCode] = '" + req.PinCode + "'" + Environment.NewLine;
            strUpdate += " ,[Posit] = '" + req.Posit + "'" + Environment.NewLine;
            strUpdate += " ,[State] = '" + req.State + "'" + Environment.NewLine;
            strUpdate += " ,[LandlineNo] = '" + req.LandlineNo + "'" + Environment.NewLine;
            strUpdate += " ,[MobileNo] = '" + req.MobileNo + "'" + Environment.NewLine;
            strUpdate += " ,[DDMMYY] = '" + req.DOB + "'" + Environment.NewLine;
            strUpdate += " ,[DDDMMMYYY] = '" + req.DOJ + "'" + Environment.NewLine;
            strUpdate += ",[Religon] = '" + req.Religon + "'" + Environment.NewLine;
            strUpdate += " ,[Nationality] = '" + req.Nationality + "'" + Environment.NewLine;

            strUpdate += ",[Department] = '" + req.Department + "'" + Environment.NewLine;
            strUpdate += ",[org] = '" + req.org + "'" + Environment.NewLine;
            strUpdate += ",[type] = '" + req.type + "'" + Environment.NewLine;
            strUpdate += ",[total] = '" + req.total + "'" + Environment.NewLine;
            strUpdate += ",[Posit] = '" + req.Posit + "'" + Environment.NewLine;
            strUpdate += ",[S_BU] = '" + req.S_BU + "'" + Environment.NewLine;
            strUpdate += ",[S_OS] = '" + req.S_OS + "'" + Environment.NewLine;
            strUpdate += ",[S_Per] = '" + req.S_Per + "'" + Environment.NewLine;
            strUpdate += ",[S_Div] = '" + req.S_Div + "'" + Environment.NewLine;
            strUpdate += ",[SS_BU] = '" + req.SS_BU + "'" + Environment.NewLine;
            strUpdate += ",[SS_OS] = '" + req.SS_OS + "'" + Environment.NewLine;
            strUpdate += ",[SS_Per] = '" + req.SS_Per + "'" + Environment.NewLine;
            strUpdate += ",[SS_Div] = '" + req.SS_Div + "'" + Environment.NewLine;
            strUpdate += ",[Work] = '" + req.Work1 + "'" + Environment.NewLine;
            strUpdate += ",[Position] = '" + req.Position + "'" + Environment.NewLine;
            strUpdate += ",[Mob1] = '" + req.Mob1 + "'" + Environment.NewLine;
            strUpdate += ",[Place1] = '" + req.Place1 + "'" + Environment.NewLine;
            strUpdate += ",[Work2] = '" + req.Work2 + "'" + Environment.NewLine;
            strUpdate += ",[Position2] = '" + req.Position2 + "'" + Environment.NewLine;
            strUpdate += ",[Mobile2] = '" + req.Mobile2 + "'" + Environment.NewLine;
            strUpdate += ",[Place2] = '" + req.Place2 + "'" + Environment.NewLine;

            strUpdate += ",[RName1] = '" + req.RName + "'" + Environment.NewLine;
            strUpdate += ",[RWork1] = '" + req.RWork1 + "'" + Environment.NewLine;
            strUpdate += ",[RMob1] = '" + req.RMob1 + "'" + Environment.NewLine;
            strUpdate += ",[RPlace1] = '" + req.RPlace1 + "'" + Environment.NewLine;
            strUpdate += ",[Rname2] = '" + req.RName2 + "'" + Environment.NewLine;
            strUpdate += ",[RWork2] = '" + req.RWork2 + "'" + Environment.NewLine;
            strUpdate += ",[RMob2] = '" + req.RMob2 + "'" + Environment.NewLine;
            strUpdate += ",[RPlace2] = '" + req.RPlace2 + "'" + Environment.NewLine;


            strUpdate += "Where RagistrationId='" + 1 + "'" + Environment.NewLine; //Session["ID"].ToString() 
            bool status = SqlHelp.ForExecuteNonQuery(strUpdate);
            return status;

        }
    }
}