using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Controllers
{
    [Route("api/admin/[controller]")]
    [ApiController]
    public class ChangePasswordController : ControllerBase
    {
        private readonly IChangePasswordService service;
        public ChangePasswordController(IChangePasswordService service)
        {
            this.service = service;
        }
        // POST api/admin/change-password
        [HttpPost]
        public Response Post(ChangePasswordModel req)
        {
            var result = service.UpdatePassword(req);
            return new Response(result);
        }
    }
}
