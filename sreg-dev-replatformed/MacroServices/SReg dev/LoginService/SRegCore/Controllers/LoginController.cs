﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private ILoginService service;
        public LoginController(ILoginService service)
        {
            this.service = service;
        }
        // PUT api/login
        [HttpPost]
        public IActionResult Put(UserModel req)
        {
            DataTable dtLoginCheck = service.Login(req);
            return Ok(dtLoginCheck);
        }
    }
}
