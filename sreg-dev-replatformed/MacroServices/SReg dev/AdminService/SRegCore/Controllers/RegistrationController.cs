using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Controllers
{
    [Route("api/admin/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private readonly IRegistrationService service;
        public RegistrationController(IRegistrationService service)
        {
            this.service = service;
        }
        // GET api/admin/ragistration
        [HttpGet]
        public Response Get([FromQuery]int RagistrationId)
        {
            DataTable dtUpdate = service.GetRegistration(RagistrationId);
            return new Response(dtUpdate);
        }

        // PUT api/admin/ragistration
        [HttpPost]
        public Response Post(RegistrationModel req)
        {
            var result = service.CreateRegistration(req);
            return new Response(result);
        }

        // POST api/admin/ragistration
        [HttpPut]
        public Response Put(RegistrationModel req)
        {
            var result =service.UpdateRegistration(req);
            return new Response(result);
        }
    }
}
