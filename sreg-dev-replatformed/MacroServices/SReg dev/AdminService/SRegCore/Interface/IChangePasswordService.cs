using SRegCore.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SRegCore.Interface
{
    public interface IChangePasswordService
    {
         Boolean UpdatePassword(ChangePasswordModel req);
    }
}
