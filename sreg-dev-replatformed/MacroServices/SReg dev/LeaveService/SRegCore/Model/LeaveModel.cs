using System;

namespace SRegCore.Model
{
    public class LeaveModel
    {
        public string LeaveId { get; set; }
        public string Pref_Language { get; set; }
        public DateTime From { get; set; }
        public DateTime Date { get; set; }
        public DateTime To { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string charge { get; set; }
        public string org { get; set; }
        public string Reason { get; set; }
    }
}