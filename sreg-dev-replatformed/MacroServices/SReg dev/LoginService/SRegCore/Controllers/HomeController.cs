﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SRegCore.Model;
using SRegCore.Interface;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SRegCore.Controllers
{
    public partial class HomeController : Controller
    {
        private static readonly String SwaggerUrl = $@"~/swagger";
        public IActionResult Index()
        {
            return new RedirectResult(SwaggerUrl);
        }
    }
}


