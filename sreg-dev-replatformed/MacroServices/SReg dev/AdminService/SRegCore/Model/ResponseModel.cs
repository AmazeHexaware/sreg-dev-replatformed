using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SRegCore.Model
{
    public class Response 
    {
        public Response( string message, string code, dynamic data=null)
        {
            this.Code = code;
            this.Message = message;
            this.Data = data;
        }
        public Response(dynamic data)
        {
            this.Data = data;
        }
        public string Code {get;set;}
        public string Message {get;set;}
        public dynamic Data {get;set;}
    }
}