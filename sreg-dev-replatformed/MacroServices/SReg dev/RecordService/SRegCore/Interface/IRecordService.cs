﻿using SRegCore.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SRegCore.Interface
{
    public interface IRecordService
    {
        Boolean UpdateRecord(Int32 Id, RecordModel req);
        Boolean CreateRecord(RecordModel req);
    }
}
