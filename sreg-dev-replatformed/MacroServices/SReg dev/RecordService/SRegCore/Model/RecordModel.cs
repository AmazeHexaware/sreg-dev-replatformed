using System;

namespace SRegCore.Model
{
    public class RecordModel
    {
        public string Pref_Language { get; set; }
        public string RecordId { get; set; }
        public DateTime Date { get; set; }
        public string Cname { get; set; }
        public string Branch { get; set; }
        public string ContactPerson { get; set; }
        public string Designation { get; set; }
        public string Sector1 { get; set; }
        public string Sector2 { get; set; }
        public string LandlineNo0 { get; set; }
        public string LandlineNo1 { get; set; }
        public string MobileNo0 { get; set; }
        public string MobileNo1 { get; set; }
        public string MobileNo2 { get; set; }
        public string FaxNo { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string EmailId { get; set; }
        public string Website { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
        public string Remark3 { get; set; }
    }
}