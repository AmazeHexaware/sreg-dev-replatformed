using SRegCore.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SRegCore.Interface
{
    public interface IRegistrationService
    {
        DataTable GetRegistration(int RagistrationId);
        DataTable UpdateRegistration(RegistrationModel req);
        Boolean CreateRegistration(RegistrationModel req);
    }
}
