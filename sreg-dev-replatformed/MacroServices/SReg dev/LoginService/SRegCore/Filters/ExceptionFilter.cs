using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Data;
using SRegCore.Model;

public partial class ExceptionFilter : IExceptionFilter
{
    private readonly IHostingEnvironment env;
    private readonly ILogger<ExceptionFilter> logger;

    public ExceptionFilter(IHostingEnvironment env, ILogger<ExceptionFilter> logger)
    {
        this.env = env;
        this.logger = logger;
    }

    public void OnException(ExceptionContext context)
    {
        logger.LogError(new EventId(context.Exception.HResult), context.Exception, context.Exception.Message);
        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        context.ExceptionHandled = true;
    }
}