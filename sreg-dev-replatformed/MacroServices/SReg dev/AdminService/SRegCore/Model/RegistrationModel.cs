using System;

namespace SRegCore.Model
{
    public class RegistrationModel
    {
        public string Pref_Language { get; set; }
        public string Category { get; set; }
        public string Passwrd { get; set; }
        public string RagistrationId { get; set; }
        public string BankNo { get; set; }
        public DateTime SalaryDate { get; set; }
        public string SalaryAmount { get; set; }
        public string BankName { get; set; }
        public string Name { get; set; }
        public string FName { get; set; }
        public string Sex { get; set; }
        public string PhotoName { get; set; }
        public string Dpmail { get; set; }
        public string mail { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PinCode { get; set; }
        public string State { get; set; }
        public string LandlineNo { get; set; }
        public string MobileNo { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DOJ { get; set; }
        public string Religon { get; set; }
        public string Nationality { get; set; }
        public string Department { get; set; }
        public string org { get; set; }
        public string type { get; set; }
        public string total { get; set; }
        public string Posit { get; set; }
        public string S_BU { get; set; }
        public string S_OS { get; set; }
        public Int32 S_Per { get; set; }
        public string S_Div { get; set; }
        public string SS_BU { get; set; }
        public string SS_OS { get; set; }
        public string SS_Per { get; set; }
        public string SS_Div { get; set; }
        public string Work1 { get; set; }
        public string Position { get; set; }
        public string Mob1 { get; set; }
        public string Place1 { get; set; }
        public string Work2 { get; set; }
        public string Position2 { get; set; }
        public string Mobile2 { get; set; }
        public string Place2 { get; set; }
        public string RName { get; set; }
        public string RWork1 { get; set; }
        public string RMob1 { get; set; }
        public string RPlace1 { get; set; }
        public string RName2 { get; set; }
        public string RWork2 { get; set; }
        public string RMob2 { get; set; }
        public string RPlace2 { get; set; }
    }
}