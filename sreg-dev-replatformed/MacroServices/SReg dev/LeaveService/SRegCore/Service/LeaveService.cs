﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Service
{
    public class LeaveService : ILeaveService
    {
        public Boolean CreateLeave(LeaveModel req)
        {
            string strQry = "Declare @MasterId int" + Environment.NewLine;
            strQry += "INSERT INTO [Leave]" + Environment.NewLine;
            strQry += " ([Pref_Language],[LeaveId],[From],[Date]" + Environment.NewLine;
            strQry += ",[To],[Name],[Designation],,[Department],[ charge]" + Environment.NewLine;
            strQry += ",[org],[Reason])" + Environment.NewLine;
            strQry += "Values('" + req.Pref_Language + "','" + req.LeaveId + "'," + Environment.NewLine;
            strQry += "'" + req.From + "','" + req.Date + "','" + req.To + "'," + Environment.NewLine;
            strQry += "'" + req.Name + "','" + req.Designation + "','" + req.Department + "'," + Environment.NewLine;
            strQry += "'" + req.charge + "','" + req.org + "','" + req.Reason + "')" + Environment.NewLine;
            var result = SqlHelp.ForExecuteNonQuery(strQry);
            return result;
        }

        public Boolean DeleteLeave(Int32 LeaveId)
        {
            string strQry = $"delete From Leave Where LeaveId='{LeaveId}'";
            var result = SqlHelp.ForExecuteNonQuery(strQry);
            return result;
        }

        public Boolean ChangeLeaveStatus(Int32 LeaveId)
        {
            string qry = string.Empty;
            qry += " Declare @s as bit";
            qry += $" select @s= isnull(cast(IsActive as bit),0) from Leave Where LeaveId='{LeaveId}'";
            qry += " IF @s=1";
            qry += " BEGIN";
            qry += " Set @s=0";
            qry += " End";
            qry += " Else";
            qry += " Begin ";
            qry += " Set @s=1";
            qry += " End";
            qry += $" Update Leave set IsActive=@s where LeaveId='{LeaveId}'";
            var result = SqlHelp.ForExecuteNonQuery(qry);
            return result;
        }
    }
}
