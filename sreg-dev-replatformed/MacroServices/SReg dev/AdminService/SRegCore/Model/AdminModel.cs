using System;

namespace SRegCore.Model
{
    public class ChangePasswordModel
    {
        public string AdminId { get; set; } 
        public string OldPasswrd { get; set; } 
        public string NewPasswrd { get; set; }
    }
}