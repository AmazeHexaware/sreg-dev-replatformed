﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SRegCore.Model;
using SRegCore.Interface;

namespace SRegCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecordController : ControllerBase
    {
        private readonly IRecordService service;
        public RecordController(IRecordService service)
        {
            this.service = service;
        }

        // PUT api/record
        [HttpPut("{Id}")]
        public IActionResult Put(Int32 Id, [FromBody] RecordModel req)
        {
            var result = service.UpdateRecord(Id, req);
            return Ok(result);
        }

        // POST api/record
        [HttpPost]
        public IActionResult Post(RecordModel req)
        {
            var result = service.CreateRecord(req);
            return Ok(result);
        }
    }
}
