﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["adminID"] == null)
        {
            Response.Redirect("index.aspx", false);
        }
        lblMsg.Text = "";
        if (!IsPostBack)
        {

            tbconfirmnewpass.Text = "";
            tbnewpass.Text = "";
            tboldpass.Text = "";

        }
    }

    protected void Ibtn_Update_Click(object sender, EventArgs e)
    {
 
        if (tbconfirmnewpass.Text != "" || tbnewpass.Text != "" || tboldpass.Text != "")
        {
            DataTable dtCheck = SqlHelp.GetDataTable("select * from admin where adminID='" + Session["adminID"].ToString() + "' and password='" + tboldpass.Text + "'");
            if (dtCheck.Rows.Count > 0)
            {
                if (SqlHelp.ForExecuteNonQuery("Update admin set password='" + tbnewpass.Text + "' where adminID='" + Session["adminID"].ToString() + "' and password='" + tboldpass.Text + "'") == true)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Administrator Password Changed Successfully !";
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
              
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Old Password dosn't match !";
                lblMsg.ForeColor = System.Drawing.Color.Red;
                return;
            }

        }
        else
        {
            lblMsg.Text = "Please Fill Password Details!";
        }
    }
}

    

